import Enum_difficulty from "./Enum_difficulty.js";
import Game from "./Game.js";
import {Player} from "./Player.js";

class Main {
    // Logic
    #game
    #missing
    #gamesPlayed
    #players

    // Canvas
    missingLetters = document.getElementById('MissingLetters')
    missingLettersContext = this.missingLetters.getContext('2d')


    guesses = document.getElementById('Guesses');
    guessesContext = this.guesses.getContext('2d');

    hangmanCanvas = document.getElementById('Hangman');
    hangmanContext = this.hangmanCanvas.getContext('2d');

    font = '24px Arial';
    textColor = '#000000';
    textBaseline = 'middle';
    textAlign = 'center';

    winnerMessage = document.getElementById('Winner');
    winnerContainer = document.getElementById('WinnerMessage');

    // Score board
    scoreBoard = document.getElementById('Score');

    constructor() {
        this.#gamesPlayed = 0
    }
    async createNewGame() {
        document.removeEventListener('keyup',this.handleKeyup) // remove prev event listener
        this.#game = new Game(this.getDifficulty(),this,this.#players);
        await this.#game.InitialiseWord();
        this.#game.setup();
        const word = await this.#game.getWord()
        this.#missing = Array.from(word).map((char) => (char === ' ' || char === '-' ? char : '_'));

        this.drawWord(this.#missing,this.missingLettersContext,this.missingLetters.width,this.missingLetters.height);
        this.drawHangman(0)

        document.addEventListener('keyup', this.handleKeyup);
    }
    // Getting the selected difficulty
    getDifficulty() {
        // Had to parse it to Int, otherwise it didn't work
        const difficulty = parseInt(document.querySelector('input[name=difficulty]:checked').value);
        switch (difficulty) {
            case 0:
                return Enum_difficulty.difficulties.EASY;
            case 1:
                return Enum_difficulty.difficulties.MEDIUM;
            case 2:
                return Enum_difficulty.difficulties.HARD;
            default:
                return Enum_difficulty.difficulties.MEDIUM;
        }
    }

    letterInput(letter) {
        this.#game.handleLetter(letter)
    }

    // Tried to do something with the local storage API, but after implementation the app couldn't be used in browser
    getExistingPlayers() {
        let i = 0;
        while (true){
            let name = '';
            i = 1;
            name = localStorage.getItem(`name${i}`);
            if (name === null){
                break;
            }
            let player = new Player();
            player.score = parseInt(localStorage.getItem(`score${i}`));
            this.#players.push(player);
            ++i;
        }
    }

    createPlayers(playerNames) {
        let num = 1;
        playerNames.forEach((name) => {
            let player;
            if (name === ""){
                // Create a new player if nothing was in input
                player = new Player(`Player ${num}`)
            } else {
                // Create a new player with custom name
                player = new Player(name);
            }
            // Store the player in an array
            this.#players.push(player);
            ++num
        });
    }

    drawWord(word,context,width,height) {
        context.clearRect(0,0,width,height)
        context.font = this.font;
        context.fillStyle = this.textColor;
        context.textBaseline = this.textBaseline;
        context.textAlign = this.textAlign;

        const letterWidth = width/word.length;

        for (let i = 0; i < word.length; i++) {
            const x = i * letterWidth + letterWidth / 2;
            const y = height / 2;

            context.fillText(word[i], x, y);
        }
    }

    get game() {
        return this.#game
    }

    // add the pressed key char to the array of missing letters
    setMissing(index,key) {
        this.#missing[index] = key;
        this.drawWord(this.#missing,this.missingLettersContext,
            this.missingLetters.width,this.missingLetters.height);
    }
    // probably not needed
    deleteGame() {
        this.#game = undefined;
        this.#missing.splice(0,this.#missing.length)
        localStorage.clear()
    }

    handleKeyup = (event) => {
        if (active){
            const key = event.key;
            const lowercaseLetterPattern = /^[a-z]$/; // pattern for all lover case letters
            if (typeof key === 'string' && lowercaseLetterPattern.test(key)) {
                main.letterInput(key); // handle input
            }
        }
    };

    createScoreBoard() {
        for (const player of this.#players){
            // create elements
            const playerContainer = document.createElement('div');
            const name = document.createElement('p');
            const score = document.createElement('p');
            // give them corresponding text
            name.innerText = player.getName(); // display name of player
            score.innerText = player.getScore(); // display score of player (could just be = 0 but this looks better)
            playerContainer.id = 'cnt' + player.getName();
            playerContainer.className = 'cnt';
            // append elements
            playerContainer.appendChild(name);
            playerContainer.appendChild(score);
            this.scoreBoard.appendChild(playerContainer)
        }
    }

    updateScore(player) {
        const playerContainer = document.getElementById('cnt' + player.getName());
        const score = playerContainer.lastChild;
        score.innerText = player.getScore();
    }



    drawHangman(numOfWrongs) {
        this.hangmanContext.lineWidth = 2;
        this.hangmanContext.strokeStyle = '#000000';

        switch (numOfWrongs){
            case 1:
                // Draw the base
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(10, this.hangmanCanvas.height - 10);
                this.hangmanContext.lineTo(this.hangmanCanvas.width - 10, this.hangmanCanvas.height - 10);
                this.hangmanContext.stroke();
                break;
            case 2:
                // Draw the pole
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(50, 20);
                this.hangmanContext.lineTo(50, this.hangmanCanvas.height - 10);
                this.hangmanContext.stroke();
                break;
            case 3:
                // Draw the horizontal bar
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(50, 20);
                this.hangmanContext.lineTo(150, 20);
                this.hangmanContext.stroke();
                break;
            case 4:
                // Draw the rope
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(150, 20);
                this.hangmanContext.lineTo(150, 50);
                this.hangmanContext.stroke();
                break;
            case 5:
                // Draw the head
                this.hangmanContext.beginPath();
                this.hangmanContext.arc(150, 62.5, 12.5, 0, Math.PI * 2);
                this.hangmanContext.stroke();
                break;
            case 6:
                // Draw the body
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(150, 75);
                this.hangmanContext.lineTo(150, this.hangmanCanvas.height - 40);
                this.hangmanContext.stroke();
                break;
            case 7:
                // Draw the left arm
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(150, 80);
                this.hangmanContext.lineTo(120, this.hangmanCanvas.height - 80);
                this.hangmanContext.stroke();
                break;
            case 8:
                // Draw the right arm
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(150, 80);
                this.hangmanContext.lineTo(180, this.hangmanCanvas.height - 80);
                this.hangmanContext.stroke();
                break;
            case 9:
                // Draw the left leg
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(150, this.hangmanCanvas.height - 40);
                this.hangmanContext.lineTo(120, this.hangmanCanvas.height - 15);
                this.hangmanContext.stroke();
                break;
            case 10:
                // Draw the right leg
                this.hangmanContext.beginPath();
                this.hangmanContext.moveTo(150, this.hangmanCanvas.height - 40);
                this.hangmanContext.lineTo(180, this.hangmanCanvas.height - 15);
                this.hangmanContext.stroke();
                break;
            default:
                // Clear canvas
                this.hangmanContext.clearRect(0,0,this.hangmanCanvas.width,this.hangmanCanvas.height)
                return;
        }
    }

    async endingAchieved() {
        active = false;
        if (this.#gamesPlayed == 5) { // change this value to adjust the length of the game
            let winner;
            let highestScore = -Infinity;
            for (const player of this.#players){ // find the winner
                if (player.getScore() > highestScore){
                    highestScore = player.getScore();
                    winner = player;
                }
            }
            this.winnerMessage.innerText = winner.getName() + ' is the winner!\n With the score: ' + winner.getScore();
            this.winnerContainer.style.display = 'block';
            playWinAudio();
            this.#gamesPlayed = 0 // reset games counter
        } else {
            ++this.#gamesPlayed;
            await this.createNewGame();
            active = true
        }
    }

    failAchieved() {
        active = false;
        this.winnerContainer.style.display = 'block';
        playLoseAudio();
    }

    set players(value) {
        this.#players = value;
    }

    get players() {
        return this.#players
    }
}
const difficulty = localStorage.getItem('difficulty'); // again tried to do something with local storage

// display the pop-up for game creation
document.addEventListener('DOMContentLoaded', () => {
    const popUp = document.getElementById('FormSection');

    // Show the pop-up
    popUp.style.display = 'block';

    const submitButton = document.getElementById('Submit');

    // Hide the pop-up when submit button is clicked
    submitButton.addEventListener('click', () => {
        popUp.style.display = 'none';
    });
});

// create Main object
const main = new Main();


// Buttons set up
const submit = document.getElementById('Submit')
const newGame = document.getElementById('New')
const continueButton = document.getElementById('Continue');
const close = document.getElementById('Close')

// Don't detect the user key input
let active = false;

// Buttons functions
submit.onclick = async function () {
    active = true; // start detecting the input
    main.players = [];
    // get player names from text inputs
    const playerInputs = Array.from(document.querySelectorAll('#inputContainer input'));
    const playerNames = playerInputs.map((input) => input.value.trim());
    // create Player objects
    main.createPlayers(playerNames);
    // create first game
    await main.createNewGame();
    // create Scoreboard
    main.createScoreBoard()
};

function newGameButton(){
    // Deactivate key input
    active = false;
    // Clear canvases
    main.missingLettersContext.clearRect(0,0,main.missingLetters.width,main.missingLetters.height);
    main.guessesContext.clearRect(0,0,main.guesses.width,main.guesses.height);
    main.hangmanContext.clearRect(0,0,main.hangmanCanvas.width,main.hangmanCanvas.height);
    main.deleteGame();
    // Display the form
    document.getElementById('FormSection').style.display = 'block';
    // Stop displaying the win/lose message
    main.winnerContainer.style.display = 'none';
    // Get Player containers
    const elements = document.getElementsByClassName('cnt')
    while (elements.length > 0) {
        // remove them all
        elements[0].parentElement.removeChild(elements[0]);
    }
}

newGame.onclick = newGameButton

continueButton.onclick = newGameButton;
// close button just so you can close the win message
close.onclick = function (){
    active = false;
    main.winnerContainer.style.display = 'none';
}

// Form

const playerContainer = document.getElementById('playerContainer')
const inputContainer = document.getElementById('inputContainer');

// Function to generate and display the input elements fo Player(s)
function generateInputElements(number) {
    inputContainer.innerHTML = '';

    for (let i = 1; i <= number; i++) {
        const input = document.createElement('input');
        input.type = 'text';
        input.id = `player${i}`;
        input.placeholder = `Player ${i}`;

        inputContainer.appendChild(input);
    }
}

// Event listener for the change event on the radio buttons in the player div
let selectedNumber;
playerContainer.addEventListener('change', function() {
    let radio = document.querySelector('input[name="players"]:checked');
    selectedNumber = radio.value;

    generateInputElements(selectedNumber);
});

//Audio
const loseAudio = document.getElementById('loseAudio');
const winAudio = document.getElementById('winAudio');
const mute = document.getElementById('muteButton');

mute.onclick = toggleMute;
// Mute function
function toggleMute() {
    if (loseAudio.muted) {
        loseAudio.muted = false;
        winAudio.muted = false
        mute.innerHTML = "Mute";
    } else {
        loseAudio.muted = true;
        winAudio.muted = true;
        mute.innerHTML = "Unmute";
    }
}
// Audion playing functions
function playLoseAudio() {
    loseAudio.play();
}

function playWinAudio(){
    winAudio.play();
}

// Local storage stuff (not working)
function storePlayers() {
    localStorage.setItem('difficulty',main.game.difficulty.toString())
    let num = 1
    for (const player of main.players){
        localStorage.setItem(`name${num}`,player.getName());
        localStorage.setItem(`score${num}`,player.getScore());
        ++num;
    }
}

window.addEventListener('beforeunload',storePlayers)