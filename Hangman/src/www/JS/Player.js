const scoreConst = 25
export class Player {
    #name
    #score
    constructor(name) {
        this.#name = name;
        this.#score = 0
    }

    increaseScore(num) {
        this.#score += num * scoreConst;
    }

    getName() {
        return this.#name;
    }

    getScore() {
        return this.#score;
    }

    // Ignore this
    set score(score){

    }
}