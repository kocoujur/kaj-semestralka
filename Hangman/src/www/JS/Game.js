import Enum_difficulty from './Enum_difficulty.js';
import {Player} from "./Player.js";

// just a function to get a random integer
function getRandomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export default class Game {
    get difficulty() {
        return this.#difficulty;
    }
    #word; // For players to guess
    #difficulty; // Selected difficulty setting
    #players // Array of players
    #activePlayer // The player whose turn it is
    #turn // To select the correct player after switching
    #guesses // Array of guessed letters
    #controller // So I can do stuff in main
    #numOfWrongs // To check if the game is over
    #numOfRights // To check if this round is over

    constructor(difficulty, controller, players) {
        this.#difficulty = difficulty;
        this.#turn = 0;
        this.#players = players;
        this.#activePlayer = this.#players[0];
        this.#guesses = [];
        this.#controller = controller;
        this.#numOfWrongs = 0;
        this.#numOfRights = 0
    }

    async InitialiseWord() {
        // While testing, I found that the longer words/phrases are easier to guess then the shorter ones
        // Comment logs to stop cheating
        switch (this.#difficulty) {
            case Enum_difficulty.difficulties.EASY:
                await this.wordGen(10, 15);
                console.log(this.#word)
                break;
            case Enum_difficulty.difficulties.MEDIUM:
                await this.wordGen(7, 10);
                console.log(this.#word)
                break;
            case Enum_difficulty.difficulties.HARD:
                await this.wordGen(4, 6);
                console.log(this.#word)
                break;
            default:
                // Probably not needed
                await this.wordGen(0, 0);
        }
    }

    // Generates the word
    async wordGen(min, max) {
        let length = '';
        for (let i = 0; i < getRandomIntFromInterval(min, max); ++i) {
            length += '?';
        }
        this.#word = await this.getApiResponse(length); // Store the word
    }

    // For the purpose of word generation I am using the https://www.datamuse.com/api/.
    getApiResponse(length) {
        return new Promise((resolve, reject) => {
            fetch('https://api.datamuse.com/words?sp=' + length) // Every ? character stands for a single random character
                .then((response) => response.json()) // Response gets sent into JSON format
                .then((data) => {
                    const randWord = data[getRandomIntFromInterval(0, 99)].word; // I take a single random entry and I take its word
                    resolve(randWord);
                })
                .catch((error) => {
                    console.error(error) // Log error
                    reject(error); // Reject error
                });
        });
    }

    // Since I'm getting the word via Promise, the getter also has to be a Promise
    getWord() {
        return new Promise((resolve) => {
            const checkWord = () => {
                if (this.#word) {
                    resolve(this.#word);
                } else {
                    setTimeout(checkWord, 100);
                }
            };
            checkWord();
        })
    }

    setup() {
        this.#controller.drawWord(this.#guesses,this.#controller.guessesContext,
            this.#controller.guesses.width,this.#controller.guesses.height)

        // Select the first player
        this.#activePlayer = this.#players[0];
    }

    switchPlayers() {
        // If else statement so that index won't be out of bounds
        if (this.#turn === this.#players.length - 1) {
            this.#turn = 0;
        } else {
            ++this.#turn;
        }
        this.#activePlayer = this.#players[this.#turn];
    }

    checkLetter(key) {
        let correct = 0
        if (this.#guesses.includes(key)) { // Return 0 correct guesses if the key was already pressed
            return correct;
        }
        // Find the key in the word
        for (let i = 0; i < this.#word.length; ++i){
            if (key === this.#word.charAt(i)) {
                this.#controller.setMissing(i,key)
                ++this.#numOfRights
                ++correct
            }
        }
        this.#guesses.push(key);
        return correct;
    }
    checkWrongs() {
        return this.#numOfWrongs === 10;
    }

    checkRights() {
        let num = 0;
        for (const char of this.#word){
            if (char === ' ' || char === '-'){ // if the word contains chars that aren't letters, they don't need to be guessed
                ++num;
            }
        }
        return this.#numOfRights === this.#word.length - num;
    }
    end() {
        this.#controller.endingAchieved();
    }

    handleLetter(letter) {
        const correct = this.checkLetter(letter)
        // if a correct key was pressed increase score, else number of incorrect guesses is increased
        if (correct !== 0) {
            this.#activePlayer.increaseScore(correct)
            this.#controller.updateScore(this.#activePlayer)
        } else {
            ++this.#numOfWrongs;
            this.#controller.drawHangman(this.#numOfWrongs)
        }
        // update guesses canvas
        this.#controller.drawWord(this.#guesses,this.#controller.guessesContext,
            this.#controller.guesses.width,this.#controller.guesses.height)
        // check if the game should end
        if (this.checkRights()) {
            this.end();
            return;
        }
        if (this.checkWrongs()){
            this.failEnd();
            return;
        }
        this.switchPlayers()
    }
    // Fail ending
    failEnd(){
        this.#controller.winnerMessage.innerText = this.#activePlayer.getName() + ' ruined it for everybody.';
        this.#controller.failAchieved();
    }
}