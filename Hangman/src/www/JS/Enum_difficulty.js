export default class Enum_difficulty{
    static difficulties = {
        EASY: Symbol(0),
        MEDIUM: Symbol(1),
        HARD: Symbol(2)
    }
}