# KAJ SEMESTRALKA



## Ciel projektu

Jednoduchá webová aplikácia pre hranie hry obesenca. 

## Postup

Po spustení aplikácie sa otvorí okno pre zvolenie počtu a pomenovanie hráčov, a zvolenie obtiažnosti. Po stlačení tlačidla "Submit" sa spustí hra. Aplikáciu je možné ovládať jednine pomocou klávesnice. Aplikácia vníma stlačenie __iba malých písmen__, všetky ostatné klávesy sú ignorované (pre úspešné dokončenie hry nie sú potrebné). Jedna séria hier pozostáva z 5 hier. Počas série hier sa na pravej strane obrazovky zobrazuje skóre jednotlivý hráčov. Po úspešnom dokončení série hier je možné vytvoriť novú sériu, zároveň sa zobrazí meno hráča s najvýším skóre. Po néuspešnom zakončení hry sa zobrazí obdobné okno, ale miesto výhercu sa zobrazí meno hráča, ktorý bol ako posledný na ťahu. Pre neúspešné zakončenie stačí nesprávne hádať 10 krát. __POZOR__ Hra bere stlačenie už použitého písmena ako nesprávny pokus! Pri oboch zakončeniach sa spustí audio, ktoré perfektne pasuje k danej situácii (pre vlasné dobro odporúčam buť nastaviť si na PC audio na 25% alebo stlačiť tlačidlo "mute" na obrazovke). 

## Popis funkčnosti

- Ako bolo vyšie spomenuté hra sa ovláda jedine pomocou klávesnice
- Na obrazovke, vpravo je tlačidlo pre spustenie novej hry
- Pod tlačítkom novej hry je tlačítko pre stlmenie/odtlmenie zvukov
- Pre podvádzanie je možné otvoriť konzolu, kde sa po spustení každej hry zobrazí slovo, ktoré je potrebné uhádnuť
- Aplikácia využíva https://www.datamuse.com/api/ pre generovanie náhodného slova. Bez pripojenia na internet nebude aplikácia fungovať.
